package com.enderun.libraryman;

import org.junit.Assert;
import org.junit.Test;

import com.enderun.libraryman.core.Author;
import com.enderun.libraryman.core.Library;
import com.enderun.libraryman.core.Member;
import com.enderun.libraryman.core.Publisher;
import com.enderun.libraryman.core.inventory.Book;
import com.enderun.libraryman.core.inventory.Cd;
import com.enderun.libraryman.core.inventory.Newspaper;


public class TestBooks {

	@Test
	public void testBookCountZero(){
		Library library = new Library();
		int bookCount = library.bookCount();
		
		Assert.assertEquals(0, bookCount);
		
	}
	
	@Test
	public void testBookCountOne(){
		Library library = new Library();
		
		Author author = new Author();
		author.setName("George Orwell");
		
		Publisher publisher = new Publisher();
		publisher.setName("Jaguar Kitapçılık");

		Book book_1984 = new Book();
		book_1984.setName("1984");
		book_1984.setAuthor(author);
		book_1984.setIsbn("123456789");
		book_1984.setPublisher(publisher);
		
		library.addBook(book_1984);
		
		int bookCount = library.bookCount();
		
		Assert.assertEquals(1, bookCount);
		
	}
	
	@Test
	public void testAddMember(){
		Library library = new Library();
		
		Author author = new Author();
		author.setName("George Orwell");
		
		Publisher publisher = new Publisher();
		publisher.setName("Jaguar Kitapçılık");
		
		Book book_1984 = new Book();
		book_1984.setName("1984");
		book_1984.setAuthor(author);
		book_1984.setIsbn("123456789");
		book_1984.setPublisher(publisher);
		
		library.addBook(book_1984);
		
		
		Member member = new Member();
		member.setName("Sinem");
		
		library.addMember(member);
		
		Assert.assertEquals(1, library.memberCount());
	}
	@Test
	public void testCdCount(){
		Library library = new Library();
		
		Cd cd=new Cd();
	    cd.setName("Hayvanlar Alemi");
		library.addCd(cd);
	
		Assert.assertEquals(1,library.CdCount());
	}
	@Test
	public void testNewspaperCount(){
		Library library = new Library();
		
		Newspaper newspaper=new Newspaper("Sabah");
		Newspaper newspaper1=new Newspaper("H�rriyet");
		library.addNewspaper(newspaper);
		library.addNewspaper(newspaper1);
	
	
		Assert.assertEquals(2,library.NewspaperCount());
	}
	@Test
	public void testMembersDifferent(){
		Library library = new Library();
		 Member member = new Member();
			member.setName("Sinem");
			library.addMember(member);
    Member member2=new Member();
    member2.setName("emine");
    library.addMember(member2);
    Assert.assertNotSame(member, member2);
  
	}
	
}
