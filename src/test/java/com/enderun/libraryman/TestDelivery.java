package com.enderun.libraryman;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import com.enderun.libraryman.borrow.Borrow;
import com.enderun.libraryman.core.Author;
import com.enderun.libraryman.core.Library;
import com.enderun.libraryman.core.Member;
import com.enderun.libraryman.core.Publisher;
import com.enderun.libraryman.core.inventory.Book;


public class TestDelivery {

	@Test
	public void testDeliveredBook() {
		Library library = new Library();

		Author author = new Author();
		author.setName("George Orwell");
		
		Publisher publisher = new Publisher();
		publisher.setName("Jaguar Kitapçılık");
		Book book_1984 = new Book();
		book_1984.setName("1984");
		book_1984.setAuthor(author);
		book_1984.setIsbn("123456789");
		book_1984.setPublisher(publisher);
		library.addBook(book_1984);
		
		Book book_1984_2 = new Book();
		book_1984_2.setName("1984");
		book_1984_2.setAuthor(author);
		book_1984_2.setIsbn("123456789");
		book_1984_2.setPublisher(publisher);
		library.addBook(book_1984_2);
		
		Member member=new Member();
		member.setName("Sinem");
		
		library.borrow(book_1984, member);
		Borrow borrow=new Borrow();
		
		borrow.setDeliveryDate(new Date());
		
	    //library.deliver(borrow);//Burada s�k�nt� var
	    Assert.assertEquals(false,book_1984.isBorrowed());
	    
	}

}
