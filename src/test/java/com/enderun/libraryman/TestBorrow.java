package com.enderun.libraryman;



import org.junit.Assert;
import org.junit.Test;

import com.enderun.libraryman.core.Author;
import com.enderun.libraryman.core.Library;
import com.enderun.libraryman.core.Member;
import com.enderun.libraryman.core.Publisher;
import com.enderun.libraryman.core.inventory.Book;
import com.enderun.libraryman.core.inventory.Dvd;
import com.enderun.libraryman.core.inventory.Magazine;

public class TestBorrow {

	@Test
	public void testBorrowedBook() {
		Library library = new Library();

		Author author = new Author();
		author.setName("George Orwell");
		
		Publisher publisher = new Publisher();
		publisher.setName("Jaguar Kitapçılık");
		Book book_1984 = new Book();
		book_1984.setName("1984");
		book_1984.setAuthor(author);
		book_1984.setIsbn("123456789");
		book_1984.setPublisher(publisher);
		library.addBook(book_1984);
		
		Book book_1984_2 = new Book();
		book_1984_2.setName("1984");
		book_1984_2.setAuthor(author);
		book_1984_2.setIsbn("123456789");
		book_1984_2.setPublisher(publisher);
		library.addBook(book_1984_2);
		
		Member member=new Member();
		member.setName("Sinem");
		
		library.borrow(book_1984, member);
		Assert.assertEquals(true, book_1984.isBorrowed());
		Assert.assertEquals(false,book_1984_2.isBorrowed());
	    
		
	
	 
		
	}
	

	
	
	@Test
	public void testBorrowedDvd(){
		Library library = new Library();
		Dvd dvd=new Dvd();
		dvd.setName("hikaye");
		library.addDvd(dvd);
		Member member=new Member();
		member.setName("Sinem");
		
		library.borrow(dvd, member);
		Assert.assertEquals(true,dvd.isBorrowed());
		
	}
	@Test
	public void testBorrowedMagazine(){
		Library library = new Library();
		Magazine magazine=new Magazine();
		magazine.setName("hikaye");
		library.addMagazine(magazine);
		Member member=new Member();
		member.setName("Sinem");
		
		library.borrow(magazine, member);
		Assert.assertEquals(true,magazine.isBorrowed());
		
	}
}
