package com.enderun.libraryman.core;

import com.enderun.libraryman.core.inventory.Book;
import com.enderun.libraryman.core.inventory.Cd;
import com.enderun.libraryman.core.inventory.Encyclopaedia;
import com.enderun.libraryman.core.inventory.Newspaper;

public class Main {

	public static void main(String []args){
		
		Library library = new Library();
		
		
		Author author = new Author();
		author.setName("George Orwell");
		
		Publisher publisher = new Publisher();
		publisher.setName("Jaguar Kitapçılık");

		Book book_1984 = new Book();
		book_1984.setName("1984");
		book_1984.setAuthor(author);
		book_1984.setIsbn("123456789");
		book_1984.setPublisher(publisher);
		
		library.getBooks().add(book_1984);
		
		
		Cd cd=new Cd();
	    cd.setName("Hayvanlar Alemi");
		library.addCd(cd);
		Newspaper newspaper=new Newspaper("Sabah");
		Publisher publisher2 = new Publisher();
		publisher2.setName("Sabahyay�nevi");
		newspaper.setPublisher(publisher2);
		Newspaper newspaper1=new Newspaper("H�rriyet");
		library.addNewspaper(newspaper);
		
		library.addNewspaper(newspaper1);
	
		
		System.out.println("**K�t�phane �al���yor**");
		System.out.println("**K�t�phanedeki kitaplar �unlard�r:");
		
		for(Book book : library.getBooks()){
			System.out.println(book);
		}
		System.out.println("**K�t�phanedeki cdler �unlard�r:");
		
		for(Cd cds:library.getCd()){
			System.out.println(cds);
		}
		System.out.println("**K�t�phanedeki gazeteler �unlard�r:");
		int i=0;
		for(Newspaper newspapers : library.getNewspaper()){
			++i;
			System.out.println(i+"-"+newspapers);
		}
		  Member member=new Member();
			member.setName("Sinem");
			library.addMember(member);
       Member member2=new Member();
       member2.setName("emine");
       library.addMember(member2);
       
       for(Member members:library.getMembers()){
    	   System.out.println(members);
       }
       System.out.println("**K�t�phanedeki ansiklopediler hakk�nda bilgi:"); 
       //TODO ANSIKLOPEDI VE CILT ILISKISI ILE ILGILI BELKI BIR ORNEK YAZILABILIR ?
       Encyclopaedia encyclopaedia=new Encyclopaedia();
       library.addEncyclopedia(encyclopaedia);
	 
	}
	
	
	
	
	
	
	
	
	
	
	
}
