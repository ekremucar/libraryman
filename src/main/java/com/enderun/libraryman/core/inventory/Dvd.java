package com.enderun.libraryman.core.inventory;


public class Dvd extends Borrowable {
	private String name;
	private String isbn;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String toString(){
		return "Dvd[ name:"+this.getName()+", �sbn:"+this.getIsbn() + " ]";
	}
}
