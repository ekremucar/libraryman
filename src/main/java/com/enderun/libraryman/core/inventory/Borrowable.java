package com.enderun.libraryman.core.inventory;


public class Borrowable {
	
	private boolean borrowed;

	public boolean isBorrowed() {
		return borrowed;
	}

	public void setBorrowed(boolean borrowed) {
		this.borrowed = borrowed;
	}

}  
