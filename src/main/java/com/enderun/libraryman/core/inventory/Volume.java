package com.enderun.libraryman.core.inventory;


//Ansiklopedinin herbir cildi
public class Volume extends Borrowable {
	private String number;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
}
