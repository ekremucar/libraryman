package com.enderun.libraryman.core.inventory;


public class Cd extends Borrowable {
private String name;

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}
public String toString() {
	return "Cd  name : [" + this.getName() +" ]";
}


}
