package com.enderun.libraryman.core.inventory;

import com.enderun.libraryman.core.Author;
import com.enderun.libraryman.core.Publisher;

public class Book extends Borrowable  {

	private Author author;
	
	private Publisher publisher;
	
	private String name;
	
	private String isbn;

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


	public String toString(){
		return "Book [ name : " + this.getName() 
				 	 +  ", author : " + this.getAuthor()
				 	 +  ", isbn : " + this.getIsbn()
				 	 +  ", publisher : " + this.getPublisher()
				 	 + " ]" ;
	}

}
