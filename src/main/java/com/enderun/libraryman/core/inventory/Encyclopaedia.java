package com.enderun.libraryman.core.inventory;

import java.util.ArrayList;
import java.util.List;

public class Encyclopaedia  {

	private String name;

	private List<Volume> volumes = new ArrayList<Volume>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Volume> getVolumes() {
		return volumes;
	}

	public void setVolumes(List<Volume> volumes) {
		this.volumes = volumes;
	}
}
