package com.enderun.libraryman.core.inventory;


public class Magazine extends Borrowable {
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		return "Magazine [ name:"+this.getName()+ " ]";
	}

}
