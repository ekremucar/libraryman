package com.enderun.libraryman.core.inventory;

import com.enderun.libraryman.core.Publisher;

public class Newspaper extends Borrowable {
	private String name;
	private Publisher publisher;
	public Publisher getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	public Newspaper(String name){
		setName(name);
		getName();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String toString(){
		return "Newspaper name:"+this.getName()+",NewspaperPublisher:"+this.getPublisher();
	}
	
	

}
