package com.enderun.libraryman.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.enderun.libraryman.borrow.Borrow;
import com.enderun.libraryman.core.inventory.AudioBook;
import com.enderun.libraryman.core.inventory.Book;
import com.enderun.libraryman.core.inventory.Borrowable;
import com.enderun.libraryman.core.inventory.Cassette;
import com.enderun.libraryman.core.inventory.Cd;
import com.enderun.libraryman.core.inventory.Document;
import com.enderun.libraryman.core.inventory.Dvd;
import com.enderun.libraryman.core.inventory.Ebook;
import com.enderun.libraryman.core.inventory.Encyclopaedia;
import com.enderun.libraryman.core.inventory.Film;
import com.enderun.libraryman.core.inventory.Magazine;
import com.enderun.libraryman.core.inventory.Manuscript;
import com.enderun.libraryman.core.inventory.Newspaper;
import com.enderun.libraryman.core.inventory.Periodical;

public class Library {

	private List<Member> members = new ArrayList<Member>();

	private List<Book> books = new ArrayList<Book>();

	private List<Cd> cd = new ArrayList<Cd>();

	private List<Newspaper> newspaper = new ArrayList<Newspaper>();

	private List<Encyclopaedia> enyclopaedia = new ArrayList<Encyclopaedia>();
    
	private List<AudioBook> audioBooks = new ArrayList<AudioBook>();
	private List<Cassette> cassettes = new ArrayList<Cassette>();
	private List<Document> documens = new ArrayList<Document>();
	private List<Dvd> dvd = new ArrayList<Dvd>();
	private List<Ebook> ebooks = new ArrayList<Ebook>();
	private List<Film> films = new ArrayList<Film>();
	private List<Manuscript> manuscriptsa = new ArrayList<Manuscript>();
	private List<Periodical> periodicals = new ArrayList<Periodical>();
	private List<Magazine> magazine = new ArrayList<Magazine>();
	
	private List<Borrow> borrowedItems=new ArrayList<Borrow>();
	private List<Borrow> deliveredItems=new ArrayList<Borrow>();
	
	public void borrow(Borrowable borrowable,Member member){
		Borrow borrow=new Borrow();
		borrow.setBorrowDate(new Date());
		borrow.setBorrowable(borrowable);
	    borrow.setMember(member);
	    borrowedItems.add(borrow);
	    borrowable.setBorrowed(true);
	}
	
	public void deliver(Borrow borrow){
		
		 borrow.setDeliveryDate(new Date());
		
		 borrowedItems.remove(borrow);
		 deliveredItems.add(borrow);
		borrow.getBorrowable().setBorrowed(false);
	}
	
	public void addMagazine(Magazine magazine) {
		this.getMagazine().add(magazine);
	}
 
    
	public List<Magazine> getMagazine() {
		return magazine;
	}


	public void setMagazine(List<Magazine> magazine) {
		this.magazine = magazine;
	}


	public void addAudioBooks(AudioBook audiobook) {
		this.getAudioBooks().add(audiobook);
	}

	public void addCassettes(Cassette cassette) {
		this.getCassettes().add(cassette);
	}

	
	public void addDocuments(Document document) {
		this.getDocumens().add(document);
	}

	public void addDvd(Dvd dvd) {
		this.getDvd().add(dvd);
	}

	public void addEbooks(Ebook ebook) {
		this.getEbooks().add(ebook);
	}

	public void addFilms(Film film) {
		this.getFilms().add(film);
	}

	public void addManuscripting(Manuscript manuscript) {
		this.getManuscriptsa().add(manuscript);
	}

	public void addPeriodical(Periodical periodical) {
		this.getPeriodicals().add(periodical);
	}

	public List<Cd> getCd() {
		return cd;
	}

	public List<AudioBook> getAudioBooks() {
		return audioBooks;
	}

	public void setAudioBooks(List<AudioBook> audioBooks) {
		this.audioBooks = audioBooks;
	}

	public List<Cassette> getCassettes() {
		return cassettes;
	}

	public void setCassettes(List<Cassette> cassettes) {
		this.cassettes = cassettes;
	}

	public List<Document> getDocumens() {
		return documens;
	}

	public void setDocumens(List<Document> documens) {
		this.documens = documens;
	}

	public List<Dvd> getDvd() {
		return dvd;
	}

	public void setDvd(List<Dvd> dvd) {
		this.dvd = dvd;
	}

	public List<Ebook> getEbooks() {
		return ebooks;
	}

	public void setEbooks(List<Ebook> ebooks) {
		this.ebooks = ebooks;
	}

	public List<Film> getFilms() {
		return films;
	}

	public void setFilms(List<Film> films) {
		this.films = films;
	}

	public List<Manuscript> getManuscriptsa() {
		return manuscriptsa;
	}

	public void setManuscriptsa(List<Manuscript> manuscriptsa) {
		this.manuscriptsa = manuscriptsa;
	}

	public List<Periodical> getPeriodicals() {
		return periodicals;
	}

	public void setPeriodicals(List<Periodical> periodicals) {
		this.periodicals = periodicals;
	}

	public void setCd(List<Cd> cd) {
		this.cd = cd;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void addEncyclopedia(Encyclopaedia e) {
		this.getEnyclopaedia().add(e);
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public int bookCount() {
		return books.size();
	}

	public int CdCount() {
		return cd.size();
	}

	public int memberCount() {
		return members.size();
	}

	public void addCd(Cd cd) {
		this.getCd().add(cd);
	}

	public void addBook(Book book) {
		this.getBooks().add(book);
	}

	public void addMember(Member member) {
		this.getMembers().add(member);
	}

	public List<Newspaper> getNewspaper() {
		return newspaper;
	}

	public void setNewspaper(List<Newspaper> newspaper) {
		this.newspaper = newspaper;
	}

	public void addNewspaper(Newspaper newspaper) {
		this.getNewspaper().add(newspaper);

	}

	public int NewspaperCount() {
		return this.newspaper.size();
	}

	public List<Encyclopaedia> getEnyclopaedia() {
		return enyclopaedia;
	}

	public void setEnyclopaedia(List<Encyclopaedia> enyclopaedia) {
		this.enyclopaedia = enyclopaedia;
	}
}
