package com.enderun.libraryman.core;

public class Author {

	private String name;
private String isbn;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "Author [ name : " + this.getName() +" ]";
	}
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
}
