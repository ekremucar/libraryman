package com.enderun.libraryman.borrow;

import java.util.Date;

import com.enderun.libraryman.core.Member;
import com.enderun.libraryman.core.inventory.Borrowable;

public class Borrow {
	
	private Date borrowDate;
	
	private Date deliveryDate;	
    
	private Member member;
    
	private Borrowable borrowable;
	
	public Borrowable getBorrowable() {
		return borrowable;
	}

	public void setBorrowable(Borrowable borrowable) {
		this.borrowable = borrowable;
	}

	public Date getBorrowDate() {
		return borrowDate;
	}
	
	public void setBorrowDate(java.util.Date date) {
		this.borrowDate = (Date) date;
	}
	
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		this.member = member;
	}
	
	
}
